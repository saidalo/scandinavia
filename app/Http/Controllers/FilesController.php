<?php

namespace App\Http\Controllers;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Items;
class FilesController extends Controller
{
    public function index() 
    {
        $items = Items::paginate(20);;
        return view('main')->with(['items' => $items]);
    }
    
    public function upload(Request $request)
    {
     $validation = Validator::make($request->all(), [
        "forUpload" => "required|mimes:pdf|max:10000"
     ]);
     if($validation->passes())
     {
      $pdf = $request->file('forUpload');
      $new_name = $pdf->getClientOriginalName() . '.' . $pdf->getClientOriginalExtension();
      $item = Items::create(['name' => url('/')."/files/$new_name"]);
      $pdf->move(public_path('files'), $new_name);
      
      return response()->json([
       'success' => true,
       'message'   => 'File Upload Successfully',
       'class_name'  => 'alert-success'
      ]);
     }
     else
     {
      return response()->json([
       'success' => false,
       'message'   => $validation->errors()->all(),
       'class_name'  => 'alert-danger'
      ]);
     }
    }
}
