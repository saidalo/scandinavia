<html>
    <head>
        <title>Test Task</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link  rel = "stylesheet"
               type = "text/css"
               href = "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
               <meta name="csrf-token" content="{{ csrf_token() }}">
        <link  rel = "stylesheet"
               type = "text/css"
               href = "{{asset('css/app.css')}}" />
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Thumbnail Gallery</h1>
                </div>
                <div class="col-lg-6">
                    <form id='myForm' method='post' enctype="multipart/form-data"  class='fileUpload'>
                    <div class='row'>
                      <div class="col-lg-3">
                        <input type='submit' value='Upload' id='upload'/>
                      </div>
                      <div class="col-lg-3">
                        <input type='file' name='forUpload'/>
                      </div>
                    </div>
                </div>
            </div>
            <hr class="mt-2 mb-5">
            <div class="row text-center text-lg-left">
                @foreach($items as $item)
                <div class="col-lg-3 col-md-4 col-6 item">
                  <a href="#" class="d-block mb-4 h-100 pdf-file" id="{{$item->name}}">
                        <img class="img-fluid img-thumbnail" src="{{url('/')}}/images/pdf.png" alt="">
                      </a>
                      <p class='item-name'>{{basename($item->name)}}</p>
                </div>
                @endforeach
            </div>
            <div class='row'>
              <div class="col-lg-12 pagination"><h1>{{ $items->links() }}</h1></div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">View File</h4> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div class="modal-body">
                <div style="text-align: center;">
        <iframe src="" style="width:92%; height:80%;" frameborder="0" id="iframe"></iframe>
        </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script 
  src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
  </script>
<script src="{{asset('js/app.js')}}"></script>