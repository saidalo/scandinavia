$('button.close').on('click', function() {
  $('#myModal').modal('hide');
})
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('#myForm').on('submit', function(event) {
  event.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: "upload",
    method: 'post',
    data: formData,
    contentType: false,
    cache: false,
    processData:false,
    success: function(reply) {
      if(reply.success) {
        swal({
          icon: "success",
        });
      } else {
        alert(reply.message);
      }
    },
    error: function(reply) {
      reply = JSON.stringify(reply)
      reply = (reply.length > 300) ? reply.substring(0, 299) : reply;
      reply += "...";
      swal("Cancelled", reply, "error");
    }
  })
});
$('.md-close').on('click', function() {
    $('.md-modal').removeClass('md-show');
  });
$('.pdf-file').on('click', function() {
  var filePath = $(this).attr('id');
  $('#iframe').attr('src', filePath);
  $('#myModal').modal('show');
});
$('md-modal').on('click', function() {
  $('.md-modal').removeClass('md-show');
});